Date Popup Restrictions
-----------------------

This module allow to add day select restrictions in the date_popup widget from
the date moduel that could be displayed as custom form widget, in a entity
with date field or as a exposed filter in a view. The approach to restrict the
selectable days is done used a view display that is used as a data source of
dates that we are interested to allow selectable in our widget. Other days not
returned by this view will be unselectable.

How to install
--------------

- Enable this module as usual
- Create a view with the filter criteria that satisfy your logic for the
  allowed selectable dates, this view display must return a datetime field with
  all the dates that you want to make selectable.
- Configure the data source view for the date popup restrictions at
  admin/settings/date_popup_restrict and select the previous created view and
  display.
- Done. Test at any form that display a date_popup widget and will allow to
  select only the dates retrived by your view.

Important Notes
---------------

- When creating your data source view probably you are interested in add a date
  filter with relative date = now to avoid retrieving past dates.
- Your view could receive an contextual filter from a node view. This module
  will calculate that argument with:
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $current_nid = arg(1);
    $current_node = node_load($current_nid);
  }

Issues
------

Please post any issue you find at project issue queue.

Credits
-------

This module was sponsored by BlueSpark Labs.
