/**
 * Intercept the beforeShow event
 */
(function ($) {

Drupal.behaviors.date_popup_restrictions = {};

Drupal.behaviors.date_popup_restrictions.attach = function (context, settings) {
  // iterate all the datePopup elements to bind the click event and override options
  if (typeof Drupal.settings.date_popup_restrictions != 'undefined') {
    var hover_msg = Drupal.settings.date_popup_restrictions.hover_msg;
    for (var idx in Drupal.settings.date_popup_restrictions.selectors) {
      var selector = Drupal.settings.date_popup_restrictions.selectors[idx];
      $(selector).focus(function(e) {
        // onclick we add our function beforeShowDay callback
        $(selector+':not(.processed)', context).addClass('processed').datepicker('option', { 'beforeShowDay' :
          function(date) {
            var day = date.getDate();
            var month = date.getMonth()+1;
            var year = date.getFullYear();
            var date_str = year + "-" + month + "-" + day;

            if (date_str in Drupal.settings.date_popup_restrictions.dws) {
              sessions = Drupal.settings.date_popup_restrictions.dws[date_str];
              return [true, 'highlight', Drupal.t(hover_msg, { '@count': sessions })];
            }
            else {
              return [false, 'disabled'];
            }
          }
        });
      });
    }
  }
};
}(jQuery));
