<?php

function _date_popup_restrictions_admin_settings() {
  $form = array();
  $views = _date_popup_restrictions_get_views_ids();

  $form['date_popup_restrictions_view_source'] = array(
    '#type' => 'select',
    '#title' => 'Select a View data source',
    '#options' => $views,
    '#required' => FALSE,
    '#default_value' => variable_get('date_popup_restrictions_view_source', ''),
    '#ajax' => array(
      'callback' => '_date_popup_restrictions_admin_settings_displays',
      'wrapper' => 'view-displays',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  if (variable_get('date_popup_restrictions_view_source', NULL)) {
    $form_state['input']['date_popup_restrictions_view_source'] = variable_get('date_popup_restrictions_view_source', NULL);
    $form = $form + _date_popup_restrictions_admin_settings_displays($form, $form_state);
  }
  else {
    $form['date_popup_restrictions_view_display'] = array(
      '#markup' => '<div id="view-displays"></div>',
    );
  }

  $form['date_popup_restrictions_selectors'] = array(
    '#type' => 'textarea',
    '#title' => 'Define the elements selectors that must apply the restrictions in jQuery format.',
    '#required' => FALSE,
    '#default_value' => variable_get('date_popup_restrictions_selectors', ''),
  );

  $form['date_popup_restrictions_hover_msg'] = array(
    '#type' => 'textfield',
    '#title' => 'Text message display when user hovers a calendar day.',
    '#required' => FALSE,
    '#default_value' => variable_get('date_popup_restrictions_hover_msg', ''),
  );

  $form['#validate'] = array('_date_popup_restrictions_admin_settings_validate');
   
  return system_settings_form($form);
}  

function _date_popup_restrictions_admin_settings_validate($form, $form_state) {
  $view_source = $form_state['input']['date_popup_restrictions_view_source'];
  $view_display = $form_state['input']['date_popup_restrictions_view_display'];
  if ($view_source) {
    $selected_date_field = _date_popup_restrictions_get_date_field($view_source, $view_display);
    if (!$selected_date_field) {
      form_set_error('date_popup_restrictions_view_display', t("This view display doesn't have datetime display fields."));
    }
  }
  else {
    form_set_error('date_popup_restrictions_view_source', t("To make date_popup_restrictions work you must select a dates source view."));
  }
}

function _date_popup_restrictions_admin_settings_displays($form, $form_state) {
  $form = array();
  $view_source = $form_state['input']['date_popup_restrictions_view_source'];
  if ($view_source) {
    $view_displays = _date_popup_restrictions_get_view_displays_ids($view_source);
    $form['date_popup_restrictions_view_display'] = array(
      '#type' => 'select',
      '#title' => 'Select a View Display that contain a datetime field output',
      '#options' => $view_displays,
      '#required' => true,
      '#default_value' => variable_get('date_popup_restrictions_view_display', ''),
      '#prefix' => '<div id="view-displays">',
      '#suffix' => '</div>',
    );
  }
  return $form;
}

/**  
 * Get a list of displays for a specific view 
 *  
 * @params $view_name
 *   the name of the view 
 * @return array 
 *   list of display names for the view
 */
function _date_popup_restrictions_get_view_displays_ids($view_name) {
  // build the array with views names and display_ids
  $display_ids = array();
  $view = views_get_view($view_name);  

  if ($view) {
    foreach (array_keys($view->display) as $display_id) {
      $display_ids[$display_id] = $display_id;
    }
  }
  return $display_ids;
}


function _date_popup_restrictions_get_views_ids() {
  $views = array();
  $views['-'] = t('-- Select --');
  foreach (views_get_all_views() as $key => $view) {
    if (isset($view->display)) {
      foreach (array_keys($view->display) as $display_id) {
        $selected_date_field = _date_popup_restrictions_get_date_field($view->name, $display_id);
        if ($selected_date_field) {
          $views[$key] = $view->name;
        }
      }
    }
  }
  return $views;
}
